# frozen_string_literal: true

# Domains controller
class DomainsController < ApplicationController
  before_action :set_domain, only: %i[show update destroy]

  # GET /domains
  def index
    @domains = Domain.all
  end

  # GET /domains/1
  def show
    redirect_to domains_url, notice: "You can't see this domain." if @domain.nil?
  end

  # GET /domains/update/1
  def update
    upds = [DnsrrsUpdater, HeadersUpdater, PortsUpdater, TlsconfigUpdater, WhoisUpdater]
    upds.each do |upd|
      o = upd.new(@domain)
      o.refresh
    end
    redirect_to action: 'show', id: @domain.id
  end

  # POST /domains
  def create
    @domain = Domain.new(domain_params)
    redirect_to '/', alert: 'This is not a valid URI.' and return unless domain_valid?
    @domain.save
    FinalReconJob.perform_later @domain
    redirect_to @domain, notice: 'Domain was successfully created.' and return if @domain.save
    redirect_to '/', alert: 'Error while saving domain.'
  end

  # DELETE /domains/1
  def destroy
    @domain.destroy
    respond_to do |format|
      format.html { redirect_to domains_url, notice: 'Domain was successfully destroyed.' }
    end
  end

  private

  # Only allow a list of trusted parameters through.
  def domain_params
    params.require(:domain).permit(:url)
  end

  def set_domain
    @domain = Domain.find(params[:id])
  end

  def domain_valid?
    uri = URI.parse(@domain.url)
    uri.is_a?(URI::HTTP) && !uri.host.nil?
  rescue URI::InvalidURIError
    false
  end
end
