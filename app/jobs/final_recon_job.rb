# frozen_string_literal: true

class FinalReconJob < ApplicationJob
  queue_as :default

  def perform(domain)
    clnurl = Shellwords.shellescape(domain.url)
    dn = domain.url.split('/')[2]
    dir = Dir.mktmpdir
    f = "#{dir}/#{dn}.xml"
    drop_if_exist(f)
    job = system("docker run --rm -d \
      -v #{dir}:/root/.local/share/finalrecon/dumps \
      --entrypoint /usr/bin/python3 \
      thewhiteh4t/finalrecon \
      finalrecon.py --full #{clnurl} -o xml > /dev/null")
    sleep(10.seconds) while !File.exist?(f)
    domain.description = xml_to_html(File.read(f))
    puts domain.description
    domain.save
  end

  private

  def xml_to_html(data)
    nkg = Nokogiri::XML(data)
    nkg.encoding = 'utf-8'
    nkg = nkg.xpath('//finalrecon')
    elts = {
      'finalrecon' => ['div', 'fn'],
      'modules' => ['div', 'mod'],
      'moduleName' => ['div', 'box'],
      'dataPair' => ['div', ''],
      'dataKey' => ['span','tag is-primary is-medium'],
      'dataVal' => ['li', 'dv']
    }
    elts.each do |elt|
      nkg.xpath("//#{elt[0]}").each do |el|
        el.name = elt[1][0]
        el['class'] = elt[1][1]
      end
    end
    nkg
  end

  def drop_if_exist(filename)
    f = File.new(filename,'w')
    File.delete(f) if File.exist?(f)
    f.close
  end

end
