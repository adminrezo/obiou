# A web UI for Final Recon

Obiou aims to quickly show the security status of a Website.

![Pipeline Status](https://gitlab.com/adminrezo/grepon/badges/master/pipeline.svg)

Dependencies :
  - Docker
  - Rails 6+

Installation :

- Install Docker

Photo by <a href="https://unsplash.com/@patrick_janser">Patrick Janser</a> on <a href="https://unsplash.com/s/photos/gr%C3%A9pon">Unsplash</a>

## Requirements

* Ruby version : 3.0+
* Rails : 6.1+
* Docker

## Test in development

```
rails server
```

## Deploy in production

### Manually

#### Run the bundle

```
git clone https://gitlab.com/adminrezo/obiou
cd obiou
export RAILS_ENV=production
export EDITOR=vi
rm config/credentials.yml.enc
bin/rails credentials:edit
bundle install
bundle exec rake assets:precompile
bundle exec rake assets:clean
bundle exec rake db:migrate
bin/rails s
```

Then go to <http://127.0.0.1:3000>.

Please use it behind a reverse proxy like Apache or Nginx.


### With Docker and Docker Compose

```
docker-compose up -d
```


