class AddDescriptionToDomains < ActiveRecord::Migration[6.1]
  def change
    add_column :domains, :description, :text
  end
end
