#####################
# Final Recon build #
#####################

FROM python AS finalrecon

RUN useradd nico -m
USER nico
WORKDIR /home/nico
RUN git clone https://github.com/thewhiteh4t/FinalRecon.git                                              
WORKDIR FinalRecon
RUN pip3 install -r requirements.txt


#####################
#  Obiou app build  #
#####################

FROM ruby AS obiou-app

ENV RAILS_ENV production
ENV EDITOR=vi

WORKDIR /home/obiou
COPY . /home/obiou
RUN bundle install
RUN curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt install nodejs
RUN rm nodesource_setup.sh
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn
RUN yarn install --check-files
RUN rm config/credentials.yml.enc
RUN bin/rails credentials:edit
RUN rake db:migrate
RUN bin/rails webpacker:compile

EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]
