#!/usr/bin/env bash

myexit=0
brakeman --no-pager
((myexit+=$?))
rubocop --require rubocop-rails -a
((myexit+=$?))
exit $myexit
